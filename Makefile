CC = g++
FLAGS = -g -c
OUT1 = jms_console
OUT2 = jms_coord
OBJS1 = jms_console.o help_functions.o
OBJS2 = jms_coord.o pool.o help_functions.o
PIPES = jms_in jms_out pipe_in_* pipe_out_*

console: $(OBJS1)
	$(CC) -g $(OBJS1) -o $(OUT1)

coord: $(OBJS2)
	$(CC) -g $(OBJS2) -o $(OUT2)

jms_console.o: jms_console.cpp
	$(CC) $(FLAGS) jms_console.cpp

jms_coord.o: jms_coord.cpp
	$(CC) $(FLAGS) jms_coord.cpp

pool.o: pool.cpp
	$(CC) $(FLAGS) pool.cpp

help_functions.o: help_functions.cpp
	$(CC) $(FLAGS) help_functions.cpp

clean:
	rm -f $(OBJS1) $(OBJS2) $(OUT1) $(OUT2) $(PIPES)
