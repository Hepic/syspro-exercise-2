#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include "help_functions.h"


const char* get_argument(int argc, char *argv[], const char *flag)
{
    for(int i=0; i<argc; ++i)
        if(!strcmp(argv[i], flag))
            return argv[i+1];
    
    return NULL;
}


int convert_str_to_int(const char *str)
{
    int ret = 0, i = 0;
    
    if(str[0] == '-')
        ++i;

    for(; i<strlen(str); ++i)
        ret = ret*10 + str[i] - '0';
    
    if(str[0] == '-')
        return -ret;

    return ret;
}


char* convert_int_to_str(int num)
{
    char *ret;

    if(!num)
    {
        ret = new char[10];

        memset(ret, 0, 10*sizeof(char));
        ret[0] = '0';

        return ret;
    }

    int temp = num;
    int len = 0;

    while(temp)
    {
        temp /= 10;
        ++len;
    }
    
    ret = new char[len+10];
    memset(ret, 0, (len+10)*sizeof(char));
    
    while(num)
    {
        ret[--len] = num%10 + '0';
        num /= 10;
    }

    return ret;
}


char* get_directory_path(const char *path, int id, int pid)
{
    char* directory_path = new char[MSGSIZE+10];
    char *ret;
    struct tm *real_time; 
    time_t curr_time = time(NULL);
    real_time = localtime(&curr_time);
    
    memset(directory_path, 0, (MSGSIZE+10)*sizeof(char));

    strcpy(directory_path, path);
    strcat(directory_path, "/sdi1400184_");
    strcat(directory_path, convert_int_to_str(id));
    strcat(directory_path, "_");
    strcat(directory_path, convert_int_to_str(pid));
    strcat(directory_path, "_");
    strcat(directory_path, convert_int_to_str(real_time->tm_year+1900));
    
    ret = convert_int_to_str(real_time->tm_mon+1);
    ret = fix_formation(ret); 
    strcat(directory_path, ret);

    ret = convert_int_to_str(real_time->tm_mday);
    ret = fix_formation(ret);
    strcat(directory_path, ret);
    
    strcat(directory_path, "_");

    ret = convert_int_to_str(real_time->tm_hour);
    ret = fix_formation(ret);
    strcat(directory_path, ret);
    
    ret = convert_int_to_str(real_time->tm_min);
    ret = fix_formation(ret);
    strcat(directory_path, ret);

    ret = convert_int_to_str(real_time->tm_sec);
    ret = fix_formation(ret);
    strcat(directory_path, ret);

    return directory_path;
}


char* fix_formation(char *str)
{
    if(strlen(str) == 1)
    {
        str[1] = str[0];
        str[0] = '0';
    }

    return str;
}
