#ifndef HELP_FUNCTIONS_H
#define HELP_FUNCTIONS_H

#define MSGSIZE 255

const char* get_argument(int, char*[], const char*);
int convert_str_to_int(const char*);
char* convert_int_to_str(int);
char* get_directory_path(const char*, int, int);
char* fix_formation(char*);

#endif
