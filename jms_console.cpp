#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "help_functions.h"


static const char *jms_in, *jms_out, *operation_file;
static char command[MSGSIZE+10], message[MSGSIZE+10];
static int fd_jms_in, fd_jms_out, loops;
static FILE *file;


void procedure()
{
    if(write(fd_jms_in, command, MSGSIZE) < 0) 
    {
        perror("write fifo");
        exit(EXIT_FAILURE);
    }
    
    memset(message, 0, sizeof(message));

    if(read(fd_jms_out, message, MSGSIZE) < 0)
    {
        perror("read fifo");
        exit(EXIT_FAILURE);
    }

    loops = convert_str_to_int(message); // the number which informs us, how many messages will be sent
    printf("------------------------------------------\n");

    for(int i=1; i<=loops; ++i)
    {
        if(read(fd_jms_out, message, MSGSIZE) < 0)
        {
            perror("read fifo");
            exit(EXIT_FAILURE);
        }
        
        printf("%s\n", message);
    }

    printf("------------------------------------------\n");
}



int main(int argc, char *argv[])
{
    jms_in = get_argument(argc, argv, "-w");
    jms_out = get_argument(argc, argv, "-r");
    operation_file = get_argument(argc, argv, "-o");

    if((fd_jms_in = open(jms_in, O_WRONLY)) < 0)
    {
        perror("open fifo");
        exit(EXIT_FAILURE);
    }
    
    if((fd_jms_out = open(jms_out, O_RDONLY)) < 0)
    {
        perror("open fifo");
        exit(EXIT_FAILURE);
    }
    
    if(operation_file != NULL)
    {
        file = fopen(operation_file, "r");
        memset(command, 0, sizeof(command));

        while(fgets(command, sizeof(command), file) != NULL)
        {
            command[strlen(command)-1] = '\0';
            procedure();
            memset(command, 0, sizeof(command));
        }
    }

    while(1)
    {
        memset(command, 0, sizeof(command));
        
        fgets(command, sizeof(command), stdin);
        command[strlen(command)-1] = '\0';
        
        procedure(); 
    }
    
    if(close(fd_jms_in) < 0)
    {
        perror("close fd");
        exit(EXIT_FAILURE);
    }
    
    if(close(fd_jms_out) < 0)
    {
        perror("close fd");
        exit(EXIT_FAILURE);
    }
    
    return 0;
}

