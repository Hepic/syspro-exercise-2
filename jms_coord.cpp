#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include "help_functions.h"
#include "pool.h"


static const char *path, *max_jobs_pool_str, *jms_out, *jms_in, *ptr;
static char command[MSGSIZE+10], message[MSGSIZE+10], temp[MSGSIZE+10], temp_1[MSGSIZE+10], loops[10];
static char pipe_name_in[MSGSIZE+10], pipe_name_out[MSGSIZE+10];
static int fd_jms_in, fd_jms_out, read_bytes, max_pool_len, max_job_len, cnt_pool, max_jobs_pool; 
static int job_id, pool_id, pid, status, time_duration, diff_time, curr_active, finished;
static int *pool_jobs, *fd_pipe_in, *fd_pipe_out;
static int *process_status, *process_pid, *job_status;
static time_t curr_time, *job_start;
static pid_t pool_pid;


void submit_procedure()
{
    memset(loops, 0, sizeof(loops));
    strcpy(loops, "1");
    
    if(write(fd_jms_out, loops, MSGSIZE) < 0)
    {
        perror("write fifo");
        exit(EXIT_FAILURE);
    }
    
    if(!pool_jobs[cnt_pool])
    {
        strcpy(pipe_name_in, "pipe_in_");
        strcpy(pipe_name_out, "pipe_out_");
        
        const char *ret = convert_int_to_str(cnt_pool);
        strcat(pipe_name_in, ret);
        strcat(pipe_name_out, ret);
        delete[] ret; 

        if(mkfifo(pipe_name_in, 0666) < 0)
        {
            perror("make fifo");
            exit(EXIT_FAILURE);
        }

        if(mkfifo(pipe_name_out, 0666) < 0)
        {
            perror("make fifo");
            exit(EXIT_FAILURE);
        }

        if((pool_pid = fork()) < 0)
        {
            perror("fork");
            exit(EXIT_FAILURE);
        }
         
        if(!pool_pid)
            pool(cnt_pool, pipe_name_in, pipe_name_out, max_jobs_pool, path);   
    
        else
        {
            if((fd_pipe_in[cnt_pool] = open(pipe_name_in, O_WRONLY)) < 0)
            {
                perror("open fifo");
                exit(EXIT_FAILURE);
            }

            if((fd_pipe_out[cnt_pool] = open(pipe_name_out, O_RDONLY | O_NONBLOCK)) < 0)
            {
                perror("open fifo");
                exit(EXIT_FAILURE);
            }

            process_pid[cnt_pool] = pool_pid;
            process_status[cnt_pool] = 1;
        }
    } 
   
    if(process_status[cnt_pool] == 1)
    {
        if(write(fd_pipe_in[cnt_pool], command, MSGSIZE) < 0)
        {
            perror("write fifo");
            exit(EXIT_FAILURE);
        }
    } 
    
    int pos = max_jobs_pool*cnt_pool + pool_jobs[cnt_pool];
    
    if(pos == max_job_len) // jobs reached the maximum length, so we reallocate
    {
        max_job_len += POOLSIZE;
        
        job_status = (int*)realloc(job_status, max_job_len * sizeof(int));
        job_start = (time_t*)realloc(job_start, max_job_len * sizeof(time_t));
        
        memset(job_status+(max_job_len-POOLSIZE), -1, POOLSIZE * sizeof(int));
    }
    
    time(&job_start[pos]);

    if(++pool_jobs[cnt_pool] == max_jobs_pool) // if jobs of a pool reach the maximum length then we are going to create a new pool
        ++cnt_pool;
    
    if(cnt_pool == max_pool_len) // pools reached the maximum length, so we reallocate
    {
        max_pool_len += POOLSIZE;

        pool_jobs = (int*)realloc(pool_jobs, max_pool_len * sizeof(int));
        fd_pipe_in = (int*)realloc(fd_pipe_in, max_pool_len * sizeof(int));
        fd_pipe_out = (int*)realloc(fd_pipe_out, max_pool_len * sizeof(int));
        process_status = (int*)realloc(process_status, max_pool_len * sizeof(int));
        process_pid = (int*)realloc(process_pid, max_pool_len * sizeof(int));
        
        memset(process_status+(max_pool_len-POOLSIZE), -1, POOLSIZE * sizeof(int));
    }
}


void status_suspend_resume_procedure()
{
    memset(loops, 0, sizeof(loops));
    strcpy(loops, "1");
    
    if(write(fd_jms_out, loops, MSGSIZE) < 0)
    {
        perror("write fifo");
        exit(EXIT_FAILURE);
    }

    memset(temp_1, 0, sizeof(temp_1));
    strcpy(temp_1, ptr);

    ptr = strtok(NULL, "");
    job_id = convert_str_to_int(ptr);
    pool_id = job_id / max_jobs_pool; // find the pool that locates current job

    if(process_status[pool_id] == 1)
    {
        if(write(fd_pipe_in[pool_id], command, MSGSIZE) < 0)
        {
            perror("write fifo");
            exit(EXIT_FAILURE);
        }
        
        if(!strcmp(temp_1, "suspend")  ||  !strcmp(temp_1, "resume"))
        {
            memset(message, 0, sizeof(message));
            
            if(!strcmp(temp_1, "suspend"))
                sprintf(message, "Sent suspend signal to JobID %d", job_id); 
            
            else if(!strcmp(temp_1, "resume"))
                sprintf(message, "Sent resume signal to JobID %d", job_id); 
            
            if(write(fd_jms_out, message, MSGSIZE) < 0)
            {
                perror("write fifo");
                exit(EXIT_FAILURE);
            }
        }
    }

    else
    {
        if(!strcmp(temp_1, "status"))
        {
            memset(message, 0, sizeof(message));
            sprintf(message, "JobID %d Status: Finished", job_id); 
            
            if(write(fd_jms_out, message, MSGSIZE) < 0)
            {
                perror("write fifo");
                exit(EXIT_FAILURE);
            }
        }

        else
        {   
            memset(message, 0, sizeof(message));
            strcpy(message, "Pool of that job is off");

            if(write(fd_jms_out, message, MSGSIZE) < 0)
            {
                perror("write fifo");
                exit(EXIT_FAILURE);
            }
        }
    }
}


void status_all_procedure()
{
    ptr = strtok(NULL, "");
    time_duration = -1;

    if(ptr != NULL  &&  strlen(ptr) > 0)
        time_duration = convert_str_to_int(ptr);
    
    int cnt = cnt_pool*max_jobs_pool + pool_jobs[cnt_pool]; // number of all jobs
    memset(loops, 0, sizeof(loops));
    
    char *ret = convert_int_to_str(cnt);
    strcpy(loops, ret);
    delete[] ret;
    
    if(write(fd_jms_out, loops, MSGSIZE) < 0)
    {
        perror("write fifo");
        exit(EXIT_FAILURE);
    }               

    for(int i=0; i<=cnt_pool; ++i)
    {
        for(int j=0; j<pool_jobs[i]; ++j)
        {
            job_id = i*max_jobs_pool + j;
            
            switch(process_status[i])
            {
                case 0:
                    time(&curr_time);
                    diff_time = difftime(curr_time, job_start[job_id]);
                    
                    if(time_duration == -1  ||  diff_time <= time_duration)
                    {
                        memset(message, 0, sizeof(message));
                        sprintf(message, "JobID %d Status: Finished", job_id);
                        
                        if(write(fd_jms_out, message, MSGSIZE) < 0)
                        {
                            perror("write fifo");
                            exit(EXIT_FAILURE);
                        }
                    }
                break;

                case 1:
                    memset(command, 0, sizeof(command));
                    sprintf(command, "status %d %d", job_id, time_duration); 

                    if(write(fd_pipe_in[i], command, MSGSIZE) < 0)
                    {
                        perror("write fifo");
                        exit(EXIT_FAILURE);
                    }
                break; 
            }
        }
    } 
}


void show_active_show_finished_procedure()
{
    if(!strcmp(ptr, "show-active"))
    {
        memset(loops, 0, sizeof(loops));
        strcpy(loops, convert_int_to_str(curr_active+1));
        
        if(write(fd_jms_out, loops, MSGSIZE) < 0)
        {
            perror("write fifo");
            exit(EXIT_FAILURE);
        }
        
        memset(message, 0, sizeof(message));
        strcpy(message, "Active:");

        if(write(fd_jms_out, message, MSGSIZE) < 0)
        {
            perror("write fifo");
            exit(EXIT_FAILURE);
        }
    }

    else if(!strcmp(ptr, "show-finished"))
    {
        memset(loops, 0, sizeof(loops));
        strcpy(loops, convert_int_to_str(finished+1));
        
        if(write(fd_jms_out, loops, MSGSIZE) < 0)
        {
            perror("write fifo");
            exit(EXIT_FAILURE);
        }
        
        memset(message, 0, sizeof(message));
        strcpy(message, "Finished:");

        if(write(fd_jms_out, message, MSGSIZE) < 0)
        {
            perror("write fifo");
            exit(EXIT_FAILURE);
        } 
    }

    for(int i=0; i<=cnt_pool; ++i)
    {
        switch(process_status[i])
        {
            case 0:
                if(!strcmp(ptr, "show-finished"))
                {
                    for(int j=0; j<pool_jobs[i]; ++j)
                    {
                        job_id = max_jobs_pool*i + j;
                        printf("JobID %d\n", job_id);
                    }
                }
            break;
            
            case 1:
                if(write(fd_pipe_in[i], command, MSGSIZE) < 0)
                {
                    perror("write fifo");
                    exit(EXIT_FAILURE);
                }
            break;
        }
    }
}


void show_pools_procedure()
{
    int cnt = 0;

    for(int i=0; i<=cnt_pool; ++i)
        if(process_status[i] == 1) // count active pools
            ++cnt;
    
    memset(loops, 0, sizeof(loops));
    
    char *ret = convert_int_to_str(cnt+1);
    strcpy(loops, ret);
    delete[] ret;
    
    if(write(fd_jms_out, loops, MSGSIZE) < 0)
    {
        perror("write fifo");
        exit(EXIT_FAILURE);
    }

    memset(message, 0, sizeof(message));
    strcpy(message, "Pool & Jobs:");

    if(write(fd_jms_out, message, MSGSIZE) < 0)
    {
        perror("write fifo");
        exit(EXIT_FAILURE);
    }

    for(int i=0; i<=cnt_pool; ++i)
    {
        if(process_status[i] != 1) // appear only active pools
            continue;

        cnt = 0;

        for(int j=0; j<pool_jobs[i]; ++j)
        {
            job_id = i*max_jobs_pool + j;

            if(job_status[job_id] == 1)
                ++cnt;
        }
        
        if(cnt >= 0)
        {
            memset(message, 0, sizeof(message));
            sprintf(message, "%d %d", process_pid[i], cnt);
            
            if(write(fd_jms_out, message, MSGSIZE) < 0)
            {
                perror("write fifo");
                exit(EXIT_FAILURE);
            }
        }
    }
}


void unknown_command_procedure()
{
    memset(loops, 0, sizeof(loops));
    strcpy(loops, "1");
    
    if(write(fd_jms_out, loops, MSGSIZE) < 0)
    {
        perror("write fifo");
        exit(EXIT_FAILURE);
    }

    memset(message, 0, sizeof(message));
    strcpy(message, "Unknown command");

    if(write(fd_jms_out, message, MSGSIZE) < 0)
    {
        perror("write fifo");
        exit(EXIT_FAILURE);
    }
}


void shutdown_procedure()
{
    memset(loops, 0, sizeof(loops));
    strcpy(loops, "1");
    
    if(write(fd_jms_out, loops, MSGSIZE) < 0)
    {
        perror("write fifo");
        exit(EXIT_FAILURE);
    }
    
    memset(message, 0, sizeof(message));
    
    int cnt = max_jobs_pool*cnt_pool + pool_jobs[cnt_pool];
    sprintf(message, "Served %d jobs, %d were still in progress", cnt, curr_active);
    
    if(write(fd_jms_out, message, MSGSIZE) < 0)
    {
        perror("write fifo");
        exit(EXIT_FAILURE);
    }
    
    for(int i=0; i<=cnt_pool; ++i)
    {
        if(process_status[i] == 1)
        {
            if(kill(process_pid[i], SIGTERM) < 0)
            {
                perror("kill SIGTERM");
                exit(EXIT_FAILURE);
            }
         
            if(close(fd_pipe_in[i]) < 0)
            {
                perror("close fd");
                exit(EXIT_FAILURE);
            }

            if(close(fd_pipe_out[i]) < 0)
            {
                perror("close fd");
                exit(EXIT_FAILURE);
            }
        }
    } 

    if(close(fd_jms_in) < 0)
    {
        perror("close fd");
        exit(EXIT_FAILURE);
    }
    
    if(close(fd_jms_out) < 0)
    {
        perror("close fd");
        exit(EXIT_FAILURE);
    }
    
    free(pool_jobs);
    free(fd_pipe_in);
    free(fd_pipe_out);
    free(process_status);
    free(process_pid);
    free(job_status);
    free(job_start);
    
    exit(EXIT_SUCCESS);
}



int main(int argc, char *argv[])
{
    path = get_argument(argc, argv, "-l");
    max_jobs_pool_str = get_argument(argc, argv, "-n");
    jms_out = get_argument(argc, argv, "-w");
    jms_in = get_argument(argc, argv, "-r");
    cnt_pool = 0, max_jobs_pool = convert_str_to_int(max_jobs_pool_str); 
    curr_active = 0, finished = 0;
   
    if(mkdir(path, 0766) < 0  &&  errno != EEXIST)
    {
        perror("make dir");
        exit(EXIT_FAILURE);
    }

    if(mkfifo(jms_out, 0666) < 0)
    {
        perror("make fifo");
        exit(EXIT_FAILURE);
    }

    if(mkfifo(jms_in, 0666) < 0)
    {
        perror("make fifo");
        exit(EXIT_FAILURE);
    }
    
    if((fd_jms_in = open(jms_in, O_RDONLY | O_NONBLOCK)) < 0)
    {
        perror("open fifo");
        exit(EXIT_FAILURE);
    }
    
    if((fd_jms_out = open(jms_out, O_WRONLY)) < 0)
    {
        perror("open fifo");
        exit(EXIT_FAILURE);
    }

    pool_jobs = (int*)calloc(POOLSIZE, sizeof(int));
    fd_pipe_in = (int*)calloc(POOLSIZE, sizeof(int));
    fd_pipe_out = (int*)calloc(POOLSIZE, sizeof(int));
    process_status = (int*)malloc(POOLSIZE * sizeof(int));
    process_pid = (int*)calloc(POOLSIZE, sizeof(int));
    job_status = (int*)malloc(POOLSIZE * sizeof(int));
    job_start = (time_t*)malloc(POOLSIZE * sizeof(time_t));

    memset(process_status, -1, POOLSIZE * sizeof(int));
    memset(job_status, -1, POOLSIZE * sizeof(int));
    max_pool_len = max_job_len = POOLSIZE;

    while(1)
    {
        pid = waitpid(-1, &status, WNOHANG);

        if(WIFEXITED(status)  &&  pid > 0)
        {
            for(int i=0; i<=cnt_pool; ++i)
                if(process_status[i] == 1  &&  process_pid[i] == pid)
                {
                    process_status[i] = 0;
                    break;
                }
        }

        memset(command, 0, sizeof(command));

        if((read_bytes = read(fd_jms_in, command, MSGSIZE)) < 0  &&  errno != EAGAIN) 
        {
            perror("read fifo");
            exit(EXIT_FAILURE);
        }

        if(read_bytes > 0)
        {
            strcpy(temp, command);
            ptr = strtok(temp, " ");
            
            if(!strcmp(ptr, "submit"))
                submit_procedure(); 
            
            else if(!strcmp(ptr, "status")  ||  !strcmp(ptr, "suspend")  ||  !strcmp(ptr, "resume"))
                status_suspend_resume_procedure();

            else if(!strcmp(ptr, "status-all"))
                status_all_procedure(); 

            else if(!strcmp(ptr, "show-active")  ||  !strcmp(ptr, "show-finished"))
                show_active_show_finished_procedure(); 

            else if(!strcmp(ptr, "show-pools"))
               show_pools_procedure(); 

            else if(!strcmp(ptr, "shutdown"))
                shutdown_procedure();

            else
            {
                unknown_command_procedure(); 
                continue;
            }
        }

        for(int i=0; i<=cnt_pool; ++i)
        {
            if(process_status[i] == 1)
            {
                memset(message, 0, sizeof(message));

                if((read_bytes = read(fd_pipe_out[i], message, MSGSIZE)) < 0  &&  errno != EAGAIN)
                {
                    perror("read fifo");
                    exit(EXIT_FAILURE);
                }
                
                if(read_bytes > 0)
                {
                    memset(temp_1, 0, sizeof(temp_1));
                    strcpy(temp_1, message);

                    ptr = strtok(temp_1, " ");

                    if(!strcmp(ptr, "info"))
                    {
                        ptr = strtok(NULL, " ");
                        job_id = convert_str_to_int(ptr);
                        
                        ptr = strtok(NULL, "");
                        int val = convert_str_to_int(ptr);
                        
                        if(job_status[job_id] == -1  &&  val == 1)
                            ++curr_active;

                        if(job_status[job_id] == 1  && val == 0)
                        {
                            --curr_active;
                            ++finished;
                        }

                        job_status[job_id] = val; 
                    }

                    else
                    {
                        if(write(fd_jms_out, message, MSGSIZE) < 0)
                        {
                            perror("write fifo");
                            exit(EXIT_FAILURE);
                        }
                    }
                }
            }
        }
    }
 
    return 0;
}

