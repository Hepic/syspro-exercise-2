while getopts l:c: option
do
    case "${option}"
    in
        l) MYPATH=${OPTARG};;
        c) COMMAND=${OPTARG};;
    esac
done

COUNTER=0

for elem in $COMMAND
do
    if [ $COUNTER -eq 0 ]
    then
        COMMAND=$elem
    
    elif [ $COUNTER -eq 1 ]
    then
        NUM=$elem
    fi

    COUNTER=$((COUNTER + 1))
done

if [ $COMMAND = "list" ]
then
    ls -l $MYPATH

elif [ $COMMAND = "size" ]
then
    du -h $MYPATH/*/ | sort -h | head -$NUM

elif [ $COMMAND = "purge" ]
then
    rm -r $MYPATH/*/
fi
