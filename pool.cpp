#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <signal.h>
#include <errno.h>
#include <fcntl.h>
#include "pool.h"
#include "help_functions.h"


static pid_t job_pid;
static int fd_pipe_in, fd_pipe_out, cnt_job, pos, job_id, active_time, fd_job_file_out, fd_job_file_err;
static int status, read_bytes, pid, cnt_finish, time_duration, diff_time;
static int *process_status, *process_pid, *process_inactive ;
static time_t curr_time, *process_start, *process_suspend;
static char command[MSGSIZE+10], message[MSGSIZE+10];
static char *args[MSGSIZE+10], job_file_path[MSGSIZE+10];
static char *ptr, *directory_path;


void submit_procedure(int pool_id, int max_jobs_pool, const char *path)
{
    if((job_pid = fork()) < 0)
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    
    if(!job_pid)
    {
        job_id = (pool_id*max_jobs_pool) + cnt_job;
        directory_path = get_directory_path(path, job_id, (int)getpid());
        
        // create folder and files for the job
        if(mkdir(directory_path, 0766) < 0)
        {
            perror("make dir");
            exit(EXIT_FAILURE);
        }
        
        memset(job_file_path, 0, sizeof(job_file_path));
        strcpy(job_file_path, directory_path);
        strcat(job_file_path, "/stdout_jobid");
        
        if((fd_job_file_out = open(job_file_path, O_WRONLY | O_CREAT | O_TRUNC, 0666)) < 0)
        {
            perror("open file");
            exit(EXIT_FAILURE);
        }
        
        memset(job_file_path, 0, sizeof(job_file_path));
        strcpy(job_file_path, directory_path);
        strcat(job_file_path, "/stderr_jobid");
        
        if((fd_job_file_err = open(job_file_path, O_WRONLY | O_CREAT | O_TRUNC, 0666)) < 0)
        {
            perror("open file");
            exit(EXIT_FAILURE);
        }
        
        delete[] directory_path;
        
        // output of jobs will be printed in the files
        dup2(fd_job_file_out, 1);
        dup2(fd_job_file_err, 2);

        pos = 0;
        ptr = strtok(NULL, " ");

        while(ptr != NULL)
        {
            args[pos++] = ptr;
            ptr = strtok(NULL, " ");
        }

        args[pos] = NULL;
        execvp(args[0], args);
    }
    
    else
    {
        time(&process_start[cnt_job]);

        process_pid[cnt_job] = job_pid;
        process_status[cnt_job++] = 1;
                            
        job_id = (pool_id*max_jobs_pool) + (cnt_job-1);
        
        memset(message, 0, sizeof(message));            
        sprintf(message, "info %d 1", job_id); // send info in coord that this job is active

        if(write(fd_pipe_out, message, MSGSIZE) < 0)
        {
            perror("write fifo");
            exit(EXIT_FAILURE);
        }
        
        memset(message, 0, sizeof(message));            
        sprintf(message, "JobID: %d, PID: %d", job_id, job_pid);

        if(write(fd_pipe_out, message, MSGSIZE) < 0)
        {
            perror("write fifo");
            exit(EXIT_FAILURE);
        }
    }
}


void status_procedure(int max_jobs_pool)
{
    char *ret;
    
    ptr = strtok(NULL, " ");
    job_id = convert_str_to_int(ptr);
    pos = job_id % max_jobs_pool;
    
    ptr = strtok(NULL, "");
    time_duration = -1;

    if(ptr != NULL  &&  strlen(ptr) > 0)
        time_duration = convert_str_to_int(ptr);

    memset(message, 0, sizeof(message));
    sprintf(message, "JobID %d Status: ", job_id); 

    switch(process_status[pos])
    {
        case 0:
            strcat(message, "Finished");
        break;

        case 1:
            time(&curr_time);
            active_time = difftime(curr_time, process_start[pos]) - process_inactive[pos];
            
            strcat(message, "Active (running for "); 
            
            ret = convert_int_to_str(active_time);
            strcat(message, ret);
            delete[] ret;
            
            strcat(message, " seconds)");
        break;

        case 2:
            strcat(message, "Suspended");
        break;
    }
    
    time(&curr_time);
    diff_time = difftime(curr_time, process_start[pos]);
    
    if(time_duration == -1  ||  diff_time <= time_duration)
    {   
        if(write(fd_pipe_out, message, MSGSIZE) < 0)
        {
            perror("write fifo");
            exit(EXIT_FAILURE);
        }
    }
}


void show_active_show_finished_procedure(int pool_id, int max_jobs_pool)
{
    for(int i=0; i<cnt_job; ++i)
    {
        if((!strcmp(ptr, "show-active")  &&  (process_status[i] == 1  ||  process_status[i] == 2))  ||
        (!strcmp(ptr, "show-finished")  &&  process_status[i] == 0))
        {
            memset(message, 0, sizeof(message));
            job_id = max_jobs_pool*pool_id + i;

            strcpy(message, "JobID ");
            strcat(message, convert_int_to_str(job_id));

            if(write(fd_pipe_out, message, MSGSIZE) < 0)
            {
                perror("write fifo");
                exit(EXIT_FAILURE);
            }
        }
    }
}


void suspend_procedure(int max_jobs_pool)
{
    ptr = strtok(NULL, "");
    job_id = convert_str_to_int(ptr);
    pos = job_id % max_jobs_pool;
    
    if(kill(process_pid[pos], SIGSTOP) < 0)
    {
        perror("kill SIGSTOP");
        exit(EXIT_FAILURE);
    }
}


void resume_procedure(int max_jobs_pool)
{
    ptr = strtok(NULL, "");
    job_id = convert_str_to_int(ptr);
    pos = job_id % max_jobs_pool;
    
    if(kill(process_pid[pos], SIGCONT) < 0)
    {
        perror("kill SIGCONT");
        exit(EXIT_FAILURE);
    }
}


void shutdown(int signum)
{
    for(int i=0; i<cnt_job; ++i)
        if(process_status[i] != 0)
        {
            if(kill(process_pid[i], SIGTERM) < 0)
            {
                perror("kill SIGTERM");
                exit(EXIT_FAILURE);
            }
        }
    
    if(close(fd_pipe_in) < 0)
    {
        perror("close fd");
        exit(EXIT_FAILURE);
    }

    if(close(fd_pipe_out) < 0)
    {
        perror("close fd");
        exit(EXIT_FAILURE);
    }
    
    delete[] process_status;
    delete[] process_pid;
    delete[] process_inactive;
    delete[] process_start;
    delete[] process_suspend;
    
    exit(EXIT_SUCCESS);
}


void pool(int pool_id, const char *pipe_name_in, const char *pipe_name_out, int max_jobs_pool, const char *path)
{
    cnt_job = cnt_finish = 0;
    process_status = new int[max_jobs_pool+10], process_pid = new int[max_jobs_pool+10];
    process_inactive = new int[max_jobs_pool+10];
    process_start = new time_t[max_jobs_pool+10], process_suspend = new time_t[max_jobs_pool+10];
    
    memset(process_status, -1, (max_jobs_pool+10) * sizeof(int));
    memset(process_inactive, 0, (max_jobs_pool+10) * sizeof(int));
    
    if(signal(SIGTERM, shutdown) == SIG_ERR)
    {
        perror("catch signal");
        exit(EXIT_FAILURE);
    }

    if((fd_pipe_in = open(pipe_name_in, O_RDONLY | O_NONBLOCK)) < 0)
    {
        perror("open fifo");
        exit(EXIT_FAILURE);
    }

    if((fd_pipe_out = open(pipe_name_out, O_WRONLY)) < 0)
    {
        perror("open fifo");
        exit(EXIT_FAILURE);
    }
    
    while(cnt_finish < max_jobs_pool)
    {
        pid = waitpid(-1, &status, WNOHANG | WUNTRACED | WCONTINUED);
        
        if(WIFEXITED(status)  &&  pid > 0)
        { 
            for(int i=0; i<cnt_job; ++i)
                if(process_status[i] == 1  &&  process_pid[i] == pid)
                {
                    process_status[i] = 0;
                    job_id = pool_id*max_jobs_pool + i;
                    
                    memset(message, 0, sizeof(message));            
                    sprintf(message, "info %d 0", job_id); // send info in coord that this job finished 
                     
                    if(write(fd_pipe_out, message, MSGSIZE) < 0)
                    {
                        perror("write fifo");
                        exit(EXIT_FAILURE);
                    }
                    
                    break;
                }
                
            ++cnt_finish;
        }
        
        if(WIFSTOPPED(status)  &&  pid > 0)
        {
            for(int i=0; i<cnt_job; ++i)
                if(process_status[i] == 1  &&  process_pid[i] == pid)
                {
                    time(&process_suspend[i]);
                    process_status[i] = 2;
                    break;
                }
        }

        if(WIFCONTINUED(status)  &&  pid > 0)
        {
            for(int i=0; i<cnt_job; ++i)
                if(process_status[i] == 2  &&  process_pid[i] == pid)
                {
                    time(&curr_time);
                    process_inactive[i] += difftime(curr_time, process_suspend[i]);
                    process_status[i] = 1;
                    break;
                }
        }

        memset(command, 0, sizeof(command));
        memset(args, 0, sizeof(args));
        
        if((read_bytes = read(fd_pipe_in, command, MSGSIZE)) < 0  &&  errno != EAGAIN)
        {
            perror("read fifo");
            exit(EXIT_FAILURE);
        }

        if(read_bytes <= 0)
            continue;
        
        ptr = strtok(command, " ");
        
        if(!strcmp(ptr, "submit"))
            submit_procedure(pool_id, max_jobs_pool, path); 
        
        else if(!strcmp(ptr, "status"))
            status_procedure(max_jobs_pool); 
        
        else if(!strcmp(ptr, "show-active")  ||  !strcmp(ptr, "show-finished"))
            show_active_show_finished_procedure(pool_id, max_jobs_pool);

        else if(!strcmp(ptr, "suspend"))
            suspend_procedure(max_jobs_pool); 

        else if(!strcmp(ptr, "resume"))
            resume_procedure(max_jobs_pool);
    }

    delete[] process_status;
    delete[] process_inactive;
    delete[] process_pid;
    delete[] process_start;
    delete[] process_suspend;

    exit(EXIT_SUCCESS);
}
